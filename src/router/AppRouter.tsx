import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Header from '../components/Header';
import AddPost from '../components/AddPost';
import BlogPosts from '../components/BlogPosts';
import Post from '../components/Post';
import useLocalStorage from '../hooks/useLocalStorage';
import Footer from '../components/Footer';

const AppRouter = () => {
    const [posts, setPosts] = useLocalStorage('posts', []);

    return (
        <BrowserRouter>
        <div className="container wrapper">
            <Header />
            <div className="main-content">
                <Switch>
                    <Route
                        render={(props) => (
                            <BlogPosts {...props} posts={posts} />
                        )}
                        path="/"
                        exact={true}
                    />
                    <Route
                        render={(props) => (
                        <AddPost {...props} posts={posts} setPosts={setPosts} />
                        )}
                        path="/add"
                    />
                    <Route
                        render={(props) => (
                            <Post {...props} posts={posts} />
                        )}
                        path="/blog/:id"
                        />
                    <Route component={() => <Redirect to="/" />} />
                </Switch>
                </div>
            </div>
            <Footer />
        </BrowserRouter>
    );
};

export default AppRouter;