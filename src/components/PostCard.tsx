import React from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';
import IPost from '../interfaces/IPost';
import moment from 'moment';

interface IProps {
    post: IPost;
}

const PostCard = ({ post }: IProps) => {
  const history = useHistory();

  return (
    <React.Fragment>
    { post &&
      <div className="col-md-6">
        <Card className="post flex-md-row m-2 h-md-250 box-shadow">
          <Card.Body className="d-flex flex-column align-items-start">
            <Card.Title className="post-title mb-0">
              <h3 className="mb-0">{post.title}</h3>
            </Card.Title>
            <div className="mb-1 text-muted">{moment(post.date).format('yyyy-MM-DD HH:mm')}</div>
            <Card.Text>
              {post.preamble}
            </Card.Text>
            <Button variant="outline-dark" onClick={() => history.push(`/blog/${post.id}`)}>Read more..</Button>
          </Card.Body>
        </Card>
      </div>
      
    }    
    </React.Fragment>
  );
};

export default PostCard;