import React from 'react';

const Footer = () => {
  return (
    <footer className="blog-footer">
        <p>Blog built by Victor Bishti</p>
    </footer>
  );
};

export default Footer;