import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
  return (
    <header className="blog-header py-3">
        <div className="row flex-nowrap justify-content-between align-items-center">
            <div className="col-4 pt-1">
                <NavLink to="/" className="text-muted" activeClassName="active" exact>
                    Blog
                </NavLink>
            </div>
            <div className="col-4 text-center">
                <NavLink to="/" className="blog-header-logo text-dark">
                    My blog
                </NavLink>
            </div>
            <div className="col-4 d-flex justify-content-end align-items-center">
            <NavLink to="/add" className="text-muted" activeClassName="active">
                Create post
            </NavLink>
            </div>
      </div>
    </header>
  );
};

export default Header;