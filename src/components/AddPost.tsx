import React from 'react';
import IPost from '../interfaces/IPost';
import CreatePostForm from './CreatePostForm';
import axios from 'axios';

interface IProps {
    history: any;
    posts: any[];
    setPosts: any;
}

const AddPost = ( {history, posts, setPosts }: IProps ) => {

    const handleOnSubmit = (post: IPost) => {
        setPosts([post, ...posts]);
        
        axios.post('http://localhost:59358/blog', post)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            });
            
        history.push(`blog/${post.id}`);
    };

    return (
        <React.Fragment>
            <CreatePostForm handleOnSubmit={handleOnSubmit} />
        </React.Fragment>
    );
};

export default AddPost;