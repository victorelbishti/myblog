import React from 'react';
import { useParams } from 'react-router-dom';
import IPost from '../interfaces/IPost';
import moment from 'moment';
import axios from 'axios';

interface IProps {
    history: any;
    posts: IPost[];
}

const Post = ({ history, posts }: IProps) => {
    const { id } = useParams<{ id: string; }>();
    const post = posts.find(post => post.id === id);

    axios.get(`http://localhost:59358/blog/${id}`)
        .then(response => {
            console.log(response.data);
        })
        .catch(error => {
            console.error(error);
        });

    return (
        <React.Fragment>
            { post ? (
                <div className="mt-5 mb-5">
                    <h1 className="post-title">{post.title}</h1>
                    <div className="blog-post-meta">
                        <span className="post-date">{moment(post.date).format('yyyy-MM-DD HH:mm')}</span>
                        <span className="post-author"> by <a href={"mailto:" + post.authorEmail}>{post.author}</a></span>
                    </div>
                    <div className="preamble border-bottom pb-1">
                        <p>{post.preamble}</p>
                    </div>
                    <div className="body pt-3">
                        <p>{post.bodyText}</p>
                    </div>
                </div>
                ) : (
                <div className="mt-5 mb-5 text-center">
                    <h2>404 not found</h2>
                    <p>Something went wrong and we we're unable to find the post you were looking for, please try again or contact our support.</p>
                </div>
                )
            }
        </React.Fragment>
    );
  };

  export default Post;