import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import IPost from '../interfaces/IPost';
import { v4 as uuid } from 'uuid';

const CreatePostForm = (props: any) => {
  
    const titleCharLimit = 50;
    const authorCharLimit = 40;
    const preambleCharLimit = 250;

    const [post, setPost] = useState({
        id: props.post ? props.post.id : 0,
        title: props.post ? props.post.title : '',
        author: props.post ? props.post.author : '',
        authorEmail: props.post ? props.post.authorEmail : '',
        preamble: props.post ? props.post.preamble : '',
        bodyText: props.post ? props.post.bodyText : '',
        date: props.post ? props.post.date : ''
    });

    const [errorMsg, setErrorMsg] = useState('');
    const { title, author, authorEmail, preamble, bodyText } = post;

    const handleOnSubmit = (event: any) => {
        event.preventDefault();

        let errorMsg = validInputFields();

        if (errorMsg === '') {

            const post = {
                id: uuid(),
                title,
                author,
                authorEmail,
                preamble,
                bodyText,
                date: new Date()
            } as IPost;

            props.handleOnSubmit(post);
        }

        setErrorMsg(errorMsg);
    };

    const handleInputChange = (event: any) => {
        const { name, value } = event.target;
        setPost((prevState) => ({
            ...prevState,
            [name]: value
        }));
    };

    const validInputFields = () => {
        const values = [title, author, authorEmail, preamble, bodyText ];

        if ( !values.every((field) => (`${field}`.trim() !== '')) ){
            return 'Please fill out all the fields.';
        } else if ( !title.match(/^.{0,50}$/) ) {
            return 'Title cannot be more than 50 characters.';
        } else if ( !author.match(/^.{0,40}$/) ) {
            return 'Author cannot be more than 40 characters.';
        } else if ( !preamble.match(/^.{0,250}$/) ) {
            return 'Preamble cannot be more than 250 characters.';
        } else if ( !authorEmail.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) ) {
            return 'Invalid email address.'
        }

        return '';
  }

  return (
    <div className="mt-4 mb-5">
      {errorMsg && <p className="alert alert-danger text-center">{errorMsg}</p>}
      <h2>Create new post</h2>
      <Form onSubmit={handleOnSubmit} className="add-post-form">
        <Form.Group controlId="name" className="mb-3">
          <Form.Label>Title</Form.Label>
          <Form.Control
            className="input-control"
            type="text"
            name="title"
            value={title}
            placeholder="Enter title of post"
            onChange={handleInputChange}
          />
            <small className={(titleCharLimit - title.length) < 0 ? 'red' : 'green'}>
              { (titleCharLimit - title.length) < 0 ? 'Title is too long' : `${titleCharLimit - title.length} characters left.` }
            </small>
        </Form.Group>
        <div className="row mb-3">
            <Form.Group controlId="author" className="col-md-6">
                <Form.Label>Post Author</Form.Label>
                <Form.Control
                    className="input-control"
                    type="text"
                    name="author"
                    value={author}
                    placeholder="Enter name of author"
                    onChange={handleInputChange}
                />
            
                <small className={(authorCharLimit - author.length) < 0 ? 'red' : 'green'}>
                { (authorCharLimit - author.length) < 0 ? 'Author name is too long' : `${authorCharLimit - author.length} characters left.` }
                </small>
            </Form.Group>
            <Form.Group controlId="authorEmail" className="col-md-6">
                <Form.Label>Author email</Form.Label>
                <Form.Control
                    className="input-control"
                    type="email"
                    name="authorEmail"
                    value={authorEmail}
                    placeholder="Enter email of author"
                    onChange={handleInputChange}
                />
            </Form.Group>
        </div>
        <Form.Group controlId="preamble" className="mb-3">
          <Form.Label>Post preamble</Form.Label>
          <Form.Control as="textarea"
            className="input-control"
            name="preamble"
            value={preamble}
            placeholder="Enter preamble of post"
            onChange={handleInputChange}
          />
            <small className={(preambleCharLimit - preamble.length) < 0 ? 'red' : 'green'}>
              { (preambleCharLimit - preamble.length) < 0 ? 'Preamble is too long' : `${preambleCharLimit - preamble.length} characters left.` }
            </small>
        </Form.Group>
        <Form.Group controlId="bodyText">
          <Form.Label>Post text</Form.Label>
          <Form.Control as="textarea" rows={10}
            className="input-control"
            name="bodyText"
            value={bodyText}
            placeholder="Enter text of post"
            onChange={handleInputChange}
          />
        </Form.Group>
        <Button variant="primary" type="submit" className="submit-btn w-100 p-3 mt-3">
          Create post
        </Button>
      </Form>
    </div>
  );
};

export default CreatePostForm;