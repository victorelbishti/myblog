import React from 'react';
import { NavLink } from 'react-router-dom';
import IPost from '../interfaces/IPost';
import PostCard from './PostCard';
import axios from 'axios';

interface IProps {
    posts: IPost[];
}

const BlogPosts = ({posts}: IProps) => {

    axios.get("http://localhost:59358/blog")
        .then(response => {
            console.log(response.data);
        })
        .catch(error => {
            console.error(error);
        });

    return (
        <React.Fragment>
            <div className="blog-posts d-flex flex-wrap">
            {(posts && posts.length > 0) ? (
                posts.map((post, index) => (                    
                    <PostCard key={post.id} post={post} />
                ))
            ) : (
                <div className="mt-4 mb-5">
                    <h3>No blog posts have been created yet.</h3>
                    <p>Create your first blog post <NavLink to="/add">here</NavLink>.</p>
                </div>
            )}
            </div>
        </React.Fragment>
    );
};

export default BlogPosts;