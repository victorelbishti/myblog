
interface IPost {
    id: string;
    title: string;
    preamble: string; 
    bodyText: string;
    author: string;
    authorEmail: string;
    date: Date;
}

export default IPost;